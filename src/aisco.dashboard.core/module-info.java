module aisco.dashboard.core {
    exports aisco.dashboard.core;
    exports aisco.dashboard;
    requires vmj.object.mapper;
    requires jdk.httpserver;
    requires aisco.financialreport.core;
    requires aisco.financialreport.income;
    requires java.logging;
    requires vmj.routing.route;
}