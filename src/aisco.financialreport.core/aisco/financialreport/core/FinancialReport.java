package aisco.financialreport.core;
import aisco.program.core.Program;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import java.util.*;

public interface FinancialReport {
    String getDescription(VMJExchange vmjExchange);
    HashMap<String,Object> getAmount(VMJExchange vmjExchange);
    HashMap<String,Object> getProgram(VMJExchange vmjExchange);
    HashMap<String,Object> printHeader(VMJExchange vmjExchange);
}