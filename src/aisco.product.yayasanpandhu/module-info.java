module aisco.product.yayasanpandhu {
    requires vmj.object.mapper;
    requires vmj.routing.route;

    requires aisco.program.core;
    requires aisco.program.activity;

    requires aisco.financialreport.core;
    requires aisco.financialreport.income;
    requires aisco.financialreport.expense;
}