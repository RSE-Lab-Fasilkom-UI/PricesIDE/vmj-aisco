module aisco.financialreport.expense {
    requires aisco.financialreport.core;
    exports aisco.financialreport.expense;
    requires vmj.object.mapper;
    requires vmj.routing.route;
}