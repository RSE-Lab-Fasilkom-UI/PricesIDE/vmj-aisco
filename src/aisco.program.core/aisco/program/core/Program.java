package aisco.program.core;
import java.util.*;
import vmj.routing.route.VMJExchange;

public interface Program {
    HashMap<String,Object> setExecutionDate(VMJExchange vmjExchange);
}