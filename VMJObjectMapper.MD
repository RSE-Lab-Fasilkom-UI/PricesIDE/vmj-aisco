# VMJ Object Mapper

Dokumentasi penggunaan VMJ Object Mapper.

## Getting Started

VMJ Object Mapper memiliki beberapa aset yang dapat digunakan, yaitu:

- VMJDatabaseTable
- VMJDatabaseField
- VMJDatabaseMapper
- VMJDatabaseUtil

### VMJDatabaseTable

Aset untuk menandai sebuah class yang ingin dipetakan kepada tabel pada database dengan memberikan sebuah anotasi @VMJDatabaseTable yang menerima parameter "tableName" sebagai nama tabel pada class yang ingin dipetakan.

```
@VMJDatabaseTable(tableName="financialreport_core")
public class FinancialReportImpl extends FinancialReportComponent {
```

### VMJDatabaseField

Aset untuk menandai sebuah field spesial yang ingin dipetakan kepada kolom dari sebuah tabel. Pemetaan dilakukan dengan memberikan beberapa paramater seperti:

| Parameter Ke- | Nama Parameter | Penjelasan | Tipe       |
|---------------|----------------|------------|------------|
|1| primaryKey | Field merupakan primary key | Boolean |
|2| foreignTableName | nama tabel yang dirujuk jika suatu field merupakan foreign key kepada tabel lain. | String |
|3| foreignColumnName | nama kolom dari tabel yang dirujuk jika suatu field merupakan foreign key kepada tabel lain. | String |
|4|isDelta | menandakan bahwa sebuah field merupakan field yang berasal dari delta module. | Boolean |

```
@VMJDatabaseField(foreignTableName = "program_core", foreignColumnName = "id")
public Program idProgram;
```

### VMJDatabaseMapper

Aset yang berfungsi untuk memetakan class dan field ke dalam tabel yang sesuai pada database yang dituliskan pada berkas database.properties. Beberapa method yang dimiliki, yaitu:

#### generateTable

Untuk memetakan class dan field kepada tabel di database. Parameter yang dibutuhkan:

| Parameter Ke- | Parameter | Tipe       |
|---------------|-----------|------------|
|1|fully qualified name dari class yang sudah diberikan anotasi @VMJDatabaseTable. | String |
|2|sebuah boolean yang jika diisi dengan nilai true, hanya akan memetakan field beranotasi @VMJDatabaseField dengan nilai true pada parameter ”isDelta” dan jika diisi dengan nilai false akan memetakan seluruh field beranotasi @VMJDatabaseField. | Boolean |

```
VMJDatabaseMapper.generateTable("aisco.program.core.ProgramComponent", false);
```

#### getTableColumnsNames

Untuk mengembalikan objek ArrayList of Strings yang berisi nama-nama kolom dari sebuah tabel
yang diciptakan dari sebuah class. Parameter yang dibutuhkan:

| Parameter Ke- | Parameter | Tipe       |
|---------------|-----------|------------|
|1|fully qualified name dari class yang sudah diberikan anotasi @VMJDatabaseTable. | String |
|2|sebuah boolean yang jika diisi dengan nilai true, hanya akan mengembalikan field beranotasi @VMJDatabaseField dengan nilai true pada parameter ”isDelta” dan jika diisi dengan nilai false akan mengembalikan seluruh field beranotasi @VMJDatabaseField. | Boolean |

```
VMJDatabaseMapper.getTableColumnsNames("aisco.program.core.ProgramComponent", false);
```

### VMJDatabaseUtil

Aset yang menyediakan beberapa method yang dapat digunakan untuk melaksanakan operasi sederhana terhadap database, seperti menjalankan sql query.